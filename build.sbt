enablePlugins(ScalaJSPlugin)
name := "XopaJS"
scalaVersion := "2.12.2"

//libraryDependencies += "org.scalatest" %%% "scalatest" % "3.0.1"
libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "0.9.1"
//libraryDependencies += "be.doeraene" %%% "scalajs-jquery" % "0.9.1"

//skip in packageJSDependencies := true
//jsDependencies += "org.webjars" % "jquery" % "2.1.4" / "2.1.4/jquery.js"

// This is an application with a main method
scalaJSUseMainModuleInitializer := true