# XopaJS #

XopaJS es una librería JavaScript que consta de un conjunto de utilidades necesarias para manipulación de objetos, sin requerir dependencias externas.

### Repositorio oficial de fuentes ###

* Version 1.20.1
* [Mira demos aquí](http://dos2ve.com/xopajs)

### Cómo implementar XopaJS? ###

Es tan sencillo como agregar la librería a tu proyecto

```html
<script src="js/xopa.min.js"></script>
```

Después, en un tag HTML, por ejemplo DIV, se define un módulo, el cual es la encapsulación de un contenedor del DOM.

```html
<div data-module="holaxopa"></div>
```

Por último se define el módulo dentro del contexto XopaJS y se inicializa el mismo.

```html
<script type="text/javascript">
    var module = _xopa.defModule("holaxopa");
    module.init = function (){
        alert("Bienvenido a XopaJS");
    };
</script>
```

La función init se ejecutará cuando todas las dependencias esten satisfechas.

## Demos

Para más detalle visita http://dos2ve.com/xopajs donde encontrarás ejemplos de uso e integración.