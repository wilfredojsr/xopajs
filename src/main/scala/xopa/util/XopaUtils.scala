package xopa.util

import org.scalajs.dom.raw._

import scala.scalajs.js.UndefOr
import scala.scalajs.js.annotation._
import scalajs.js

object XopaUtils {

  def matchProperty(obj: js.Any, p: String): Boolean =
    js.typeOf(obj) match {
      case `p` => true
      case _ => false
    }

  @JSExportTopLevel("_xopa.isUndefined")
  def isUndefined(obj: js.Any): Boolean = js.isUndefined(obj)

  @JSExportTopLevel("_xopa.isNull")
  def isNull(obj: js.Any): Boolean =
    obj match {
      case null => true
      case _ => false
    }

  @JSExportTopLevel("_xopa.isFunction")
  def isFunction(obj: js.Any): Boolean = matchProperty(obj, "function")

  @JSExportTopLevel("_xopa.isObject")
  def isObject(obj: js.Any): Boolean = matchProperty(obj, "object")

  @JSExportTopLevel("_xopa.isString")
  def isString(obj: js.Any): Boolean = matchProperty(obj, "string")

  @JSExportTopLevel("_xopa.isNumber")
  def isNumber(obj: js.Any): Boolean = matchProperty(obj, "number")

  @JSExportTopLevel("_xopa.isBoolean")
  def isBoolean(obj: js.Any): Boolean = matchProperty(obj, "boolean")

  @JSExportTopLevel("_xopa.isFile")
  def isFile(obj: js.Any): Boolean = matchProperty(obj, "file")

  @JSExportTopLevel("_xopa.isArray")
  def isArray(obj: js.Any): Boolean =
    !isFunction(obj) && !isUndefined(js.Object.getOwnPropertyDescriptor(obj.asInstanceOf[js.Array[js.Any]], "length"))

  @JSExportTopLevel("_xopa.isEmpty")
  def isEmpty(obj: js.Any): Boolean =
    if (isUndefined(obj) || isNull(obj)) true
    else if (isArray(obj) &&  obj.asInstanceOf[js.Array[js.Any]].length == 0) true
    else if (isString(obj) && obj.toString.isEmpty) true
    else if (isObject(obj) && js.Object.getOwnPropertyNames(obj.asInstanceOf[js.Object]).length == 0) true
    else false

  @JSExportTopLevel("_xopa.extend")
  def extend(obj1: js.Object, obj2: js.Object): js.Object = {
    if (isUndefined(obj1)) obj2
    else if (isUndefined(obj2)) obj1
    else {
      js.Object.getOwnPropertyNames(obj2) map (prop => {
        js.Object.defineProperty(obj1, prop, js.Object.getOwnPropertyDescriptor(obj2, prop))
      })
      obj1
    }
  }

  @JSExportTopLevel("_xopa.each")
  def each(array: js.Array[js.Any], f: js.Function) = {
    if (isArray(array)) {
      var i = -1;
      array takeWhile(el => {
        i = i + 1
        val band = f.call(array, i, el)
        isUndefined(band) || !isBoolean(band)
      })
    }
  }

  @JSExportTopLevel("_xopa.restful")
  def restful(opt: js.Object) = sendRequest(opt)

  @JSExportTopLevel("_xopa.rest.get")
  def restGet(opt: js.Object) = sendRequest(extend(opt, js.Dynamic.literal("method" -> "GET")))

  @JSExportTopLevel("_xopa.rest.post")
  def restPost(opt: js.Object) = sendRequest(extend(opt, js.Dynamic.literal("method" -> "POST")))

  @JSExportTopLevel("_xopa.rest.put")
  def restPut(opt: js.Object) = sendRequest(extend(opt, js.Dynamic.literal("method" -> "PUT")))

  @JSExportTopLevel("_xopa.rest.delete")
  def restDelete(opt: js.Object) = sendRequest(extend(opt, js.Dynamic.literal("method" -> "DELETE")))

  @JSExportTopLevel("_xopa.sendRequest")
  def sendRequest(opt: js.Object) = {
    val options = js.Dynamic.literal(
      "method" -> "GET",
      "url" -> "",
      "contentType" -> "application/x-www-form-urlencoded; charset=UTF-8",
      "async" -> true,
      "timeout" -> 30000,
      "data" -> js.undefined,
      "done" -> js.undefined,
      "fail" -> js.undefined
    )
    extend(options, opt)
    if (options.data.isInstanceOf[js.Object]) options.contentType = "application/json"
    val xhr = new XMLHttpRequest()

    xhr.open(
      options.method.toString,
      options.url.toString,
      options.async.asInstanceOf[Boolean]
    )
    xhr.setRequestHeader("Content-Type", options.contentType.toString)
    xhr.onload = { (e: Event) =>
      if (xhr.status == 200) {
        val contentTypes = Array("application/json", "text/plain")
        val contentType = xhr.getResponseHeader("Content-Type")

        contentTypes.find(contentType.contains(_)).getOrElse(js.undefined) match {
          case t: String => {
            if (isFunction(options.done)) {
              if (t == contentTypes.head) options.done.call(0, js.JSON.parse(xhr.response.toString))
              else if (t == contentTypes(1)) options.done.call(0, xhr.response.toString)
            }

          }
          case _ => println("Respsonse type has not supported: " + xhr.getResponseHeader("Content-Type"))
        }
      } else {
        if (isFunction(options.fail))
          options.fail.call(0, xhr.response.toString)
      }
    }
    xhr.onerror =
      (e: Event) => if (isFunction(options.fail)) options.fail.call(0, "Error sending request.") else println("Error sending request.")

    if (XopaUtils.isEmpty(options.data)) xhr.send()
    else {
      if (options.contentType.toString contains "json")
        xhr.send(js.JSON.stringify(options.data))
      else xhr.send(options.data)
    }
  }

}