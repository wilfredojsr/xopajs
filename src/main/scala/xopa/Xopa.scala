package xopa

import scala.scalajs.js.annotation._

@JSExportTopLevel("_xopa")
object XopaJSApp {

  def main(args: Array[String]): Unit = {
    println("XopaJS Ready!")
  }

}
