(function() {
  console.log('Checking dependences');
  var allDepends = false;
  var f = function() {
    if (_xopa != undefined
    && _xopa.serialize != undefined) {
    allDepends = true
    console.log('Initialize modules [calling _xopa.init]');
    _xopa.init();
    }
  }
  var interval = function() {
    setTimeout(function() {
      f();
      if (!allDepends) interval();
    }, 250);
  }
  interval();
})();