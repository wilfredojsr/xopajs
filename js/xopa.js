(function () {
  window._xopa = window._xopa || {module: []};
  window._xopa.init = function() {
    var len = Object.keys(_xopa.module).length;
    if (len > 0) {
      var keys = Object.keys(_xopa.module);
      for (var i = 0; i < len; i++) {
        var $mod = _xopa.module[keys[i]];
        if (_xopa.isFunction($mod.init)) {
          try {
            console.log('Module -> ', $mod.name);
            $mod.init();
          } catch(e) {
            console.log('error cargando el modulo ', $mod.name)
          }
        }
      }
    }
  };
  window._xopa.defModule = function (moduleName) {
    var $mod = $('[data-module="' + moduleName + '"]');
    _xopa.module[moduleName] = _xopa.module[moduleName] || ($mod.length === 0 ? {} : $mod);
    _xopa.module[moduleName].name = moduleName;
    return _xopa.module[moduleName];
  };
  window._xopa.clean = function ($module) {
    _xopa.module[$module.name] = undefined;
  };
  _xopa.swal = function(options, ok, cancel) {
    options = options || {};
    var defaults = {
      title: "",
      text: "",
      html: true,
      type: "error",
      buttons: {
        ok: {
          text: "Volver"
        },
        cancel: {
          text: "Cancelar"
        }
      }
    }
    _xopa.extend(defaults, options);
    var $body = $(document.body);
    var $mainClients = $body.find('main.clients');
    var $modal = $('<div class="modal"><div class="shadow"></div></div>');
    var $cvContainer = $('<div class="cv-container"></div>');
    var $modalDialog = $('<div class="modal-dialog"></div>');
    var $modalContent = $('<div class="modal-content"></div>');
    var $modalHeader = $('<div class="modal-header"><button class="cancel close" type="button"><span></span></button></div>');
    var $modalBody = $('<div class="modal-body"></div>');
    var $modalBodyBtnP = $('<p class="cen-bt"></p>');
    var $modalBodyBtnOk = $('<button class="cv-btn ok close"></button>');
    var $modalBodyBtnCancel = $('<button class="cv-btn inverted cancel close"></button>');
    if(defaults.type === "oops") {
      var $typeOpps = $('<h4 class="cenbig">Oops!</h4>');
      $modal.addClass('ops');
      $modalHeader.append($typeOpps);
      $modalDialog.addClass('thiner');
      defaults.buttons.ok.text = "Aceptar";
    } else if (defaults.type === "confirm") {
      $modalBodyBtnCancel.html(defaults.buttons.cancel.text);
      $modalBodyBtnP.append($modalBodyBtnCancel);
      defaults.buttons.ok.text = "Aceptar";
      $modal.addClass('notice');
      var $headerText = $('<h4 class="lg cen"></h4>');
      $headerText.html(defaults.title);
      $modalHeader.append($headerText);
      $modalBody.addClass('sml cen');
    } else if (defaults.type === "success") {
      var $headerText = $('<h4 class="lg cen"></h4>');
      $headerText.html(defaults.title);
      $modalHeader.append($headerText);
      $modal.addClass('notice');
      $modalBody.addClass('sml cen');
    } else if(!_xopa.isEmpty(defaults.title)) {
      $modalHeader.append(defaults.title);
    }
    if (defaults.type === "loader") {
      var $loaderHeader = $('<h4 class="loading"></h4>');
      if(!_xopa.isEmpty(defaults.title))
        $loaderHeader.append(defaults.title);
      $modalHeader.html($loaderHeader);
      $modalBody.addClass('seg');
      $modalDialog.addClass('load');
    } else {
      $modalBody.html(defaults.text);
      $modalBodyBtnOk.html(defaults.buttons.ok.text);
      $modalBodyBtnP.append($modalBodyBtnOk);
      $modalBody.append($modalBodyBtnP);
    }
    $modalContent.append($modalHeader);
    $modalContent.append($modalBody);
    $modalDialog.append($modalContent);
    $cvContainer.append($modalDialog);
    $modal.append($cvContainer);
    if (defaults.type === "loader") {
      $mainClients.append($modal);
    } else {
      $body.addClass('on-pop');
      $mainClients.append($modal);
    }
    if (defaults.type === "loader") {
      $modal.css({ paddingTop: 0 + 'px', display: 'block' }).fadeIn();
    } else {
      $modal.css({ paddingTop: $body.scrollTop() + 'px', zIndex: 9999 }).fadeIn();
    }
    var onClose = function(f) {
      $body.removeClass('on-pop');
      setTimeout(function(){$modal.remove();}, 500);
      if (_xopa.isFunction(f)) f();
    }
    $modal.close = function() {
      onClose();
    };
    $modal.find('button.ok.close').on('click', function () {
      $modal.fadeOut(function () { onClose(ok); });
    });
    $modal.find('button.cancel.close').on('click', function () {
      $modal.fadeOut(function () { onClose(cancel); });
    });
    return $modal;
  };
  _xopa.loader = function(options, cancel) {
    options = options || {};
    var defaults = {title: 'cargando'};
    _xopa.extend(defaults, options);
    return _xopa.swal({type:'loader', title: defaults.title});
  };
  _xopa.serialize = function (form, options) {
    options = options || {};
    var settings = {};
    $.extend(settings, options);
    var array = [];
    var data = undefined;
    if (!_xopa.isEmpty(form)) {
      var files = form.find('input[type="file"]');
      var elements = form.find('input[type="text"], input[type="email"], input[type="number"], input[type="hidden"], '+
      'input[type="select"], select, textarea');
      var checkboxes = form.find('input[type="checkbox"]');
      var radioButtons = form.find('input[type="radio"]');
      _xopa.each(checkboxes, function (el) {
        if ($(el).is(':checked') || $(el).prop('checked')) elements.push(el);
      });
      _xopa.each(radioButtons, function (el) {
        if ($(el).is(':checked') || $(el).prop('checked')) elements.push(el);
      });
      _xopa.each(elements, function (el) {
        if ($(el).is(':disabled') || $(el).prop('checked')) array.push(el);
      });
      _xopa.each(array, function (el){
        $(el).prop('disabled',false);
      });
      if (files.length > 0) {
        data = new FormData();
        _xopa.each(files, function (el) {
          var $el = $(el);
          var file = $el[0].files[0];
          if (_xopa.isEmpty(file))
            file = new File([""], "empty.txt", {type: "text/plain", lastModified: new Date()});
          data.append($el.attr('name'), file);
        });
        _xopa.each(elements, function (el) {
          var $el = $(el);
          if (!_xopa.isEmpty($el.attr('name')) && !_xopa.isEmpty($el.val()))
            data.append($el.attr('name'), $el.val());
        });
        data.isMultipart = true;
      } else {
        data = form.serialize();
      }
      _xopa.each(array, function (el) {
        $(el).prop('disabled',true);
      });
      return data;
    }
    return false;
  };
  _xopa.sendRequest = function (options) {
    function send(options) {
      var _this = this;
      options = options || {};
      if (_xopa.isEmpty(options.method)) options.method = "POST";
      options.loader = _xopa.isEmpty(options.loader) ? false : options.loader;
      options.async = _xopa.isEmpty(options.async) ? true : options.async;
      options.timeout = _xopa.isEmpty(options.timeout) ? 300000 : options.timeout;
      options.contentType = _xopa.isEmpty(options.contentType) ? 'application/x-www-form-urlencoded; charset=UTF-8' : options.contentType;
      options.processData = _xopa.isEmpty(options.processData) ? true : false;
      var $loader;
      if (options.loader) $loader = _xopa.loader();
      if (!_xopa.isEmpty(options.data) && options.data.isMultipart) {
        options.contentType = false;
        options.processData = false;
      }
      var cid = Math.random();
      var backDrop = $('<div class="jsAjaxBackdrop" data-cid="' + cid + '" style="position: fixed; top: 0px; left:0px; width: 100%; height: 100%; z-index: 99999; cursor: wait;"></div>');
      $('body').append(backDrop);
      _this.backDrop = $('div[data-cid="' + cid + '"]');
      return $.ajax({
        method: options.method,
        url: options.url,
        async: options.async,
        timeout: options.timeout,
        contentType: options.contentType,
        processData: options.processData,
        data: options.data
      }).done(function (response) {
        _this.backDrop.remove();
        if (!_xopa.isUndefined($loader)) $loader.close();
        var isJson = true;
        var json = response;
        if (!_xopa.isObject(json)) {
          try {
            json = JSON.parse(response);
          } catch (e) {
            isJson = false;
            if (_xopa.isFunction(options.done)) options.done(response);
          }
        }
        if (!_xopa.isUndefined(options.form)) options.form.find('.has-error').removeClass('has-error');
        if (isJson) {
          _xopa.validateResponse(json, function () {
            if (_xopa.isFunction(options.done)) options.done(response);
          }, function (errors) {
          if (_xopa.isFunction(options.fail)) options.fail(errors);
          });
        }
      }).fail(function (response, status) {
        _this.backDrop.remove();
        if (!_xopa.isUndefined($loader)) $loader.close();
        var json = response;
        if (!_xopa.isObject(json)) json = JSON.parse(response);
        if (!_xopa.isUndefined(options.form)) options.form.find('.has-error').removeClass('has-error');
        _xopa.validateResponse(json, function () {}, function () {
          if (_xopa.isFunction(options.fail)) options.fail(json);
        });
      });
    }
    return new send(options);
  };
  _xopa.validateResponse = function (resp, success, errorCallback) {
    if (resp.status === 0 || resp.status === 400 || resp.status === 404) {
      setTimeout(function () {
      _xopa.swal("", resp.statusText, "error");
      }, 500);
      return true;
    }
    var json = undefined;
    if (!_xopa.isUndefined(resp.responseJSON))
      json = resp.responseJSON;
    else
      json = resp;
    if (!_xopa.isUndefined(json.error)
      && !_xopa.isUndefined(json.show_error)) {
      var text = !_xopa.isUndefined(json.error_message) ? json.error_message : '';
      _xopa.swal({type: "oops",text: text});
      if (_xopa.isFunction(errorCallback)) errorCallback();
      return false;
    }
    if (!_xopa.isUndefined(json.uri_redirect)) {
      window.location.href = json.uri_redirect;
    } else {
      if (_xopa.isFunction(success)) success();
    }
    return true;
  };
})();